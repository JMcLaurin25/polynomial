
#include "polynomial.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*Creates a new node term*/
struct term *make_term(int coeff, int exp)
{
	struct term *node = malloc(sizeof(*node));
	if (node) {
		node->coeff = coeff;
		node->exp = exp;
		node->next = NULL;
	}
	return node;
}

/*Creates and adds new term node to the linked list*/
void poly_add(polynomial **eqn, int coeff, int exp)
{
	struct term *new_node = make_term(coeff, exp);
	if (new_node) {
		new_node->next = *eqn;
		*eqn = new_node;	
	}
}

/*Frees the allocated memory for each node in the linked list.*/
void poly_free(polynomial *eqn)
{
	while (eqn) {
		struct term *tmp = eqn->next;
		free(eqn);
		eqn = tmp;
	}
}

/*Prints each term in the polynomial linked list*/
void poly_print(polynomial *eqn)
{
	if (!eqn) {
		return;
	}
	if (eqn->coeff) {
		printf("%c%d", eqn->coeff > 0 ? '+' : '\0', eqn->coeff);
		if(eqn->exp > 1) {
			printf("x^%d", eqn->exp);
		} else if (eqn->exp == 1) {
			printf("x");
		}
		//printf(" ");
	}
	poly_print(eqn->next);
}

/*Returns a newly-malloced string that displays the given polynomial*/
char *poly_to_string(polynomial *poly)
{
	if (!poly) {
		return NULL;
	}

	char *string = malloc(1);
	if (string) {
		memset(string, 0, 1);
	}
	char temp[256], sign; //Calculate better Upper limit for temp size!

	while(poly) {
		if (poly->exp > 0) {
			sign = poly->coeff < 0 ? '-' : '+';
			if (poly->exp == 1) {

				if (poly->coeff < 0) {
					snprintf(temp, sizeof(temp), "%c%d%s", sign, poly->coeff * -1, "x");
				} else {
					snprintf(temp, sizeof(temp), "%c%d%s", sign, poly->coeff, "x");
				}
			} else {
				if (poly->coeff != 0) {
					snprintf(temp, sizeof(temp), "%c%d%s%d", sign, poly->coeff, "x^", poly->exp);
				} else {
					poly = poly->next;
					continue;
				}
			}

			//2 adjusts for last character.
			string = realloc(string, (int)strlen(string) + (int)strlen(temp) + 2); 
			strncat(string, temp, strlen(temp));
			string[strlen(string) + 1] = '\0';
			} else {
				sign = poly->coeff < 0 ? '-' : '+';

				snprintf(temp, sizeof(temp), "%c%d", sign, poly->coeff);
				string = realloc(string, strlen(string) + strlen(temp) + 1);
				strncat(string, temp, strlen(temp));
			}
			poly = poly->next;
	}
	return string;
}

/*Returns a newly-malloced polynomial that is the sum of the two arguments*/
polynomial *add_poly(polynomial *poly_a, polynomial *poly_b)
{
	if (!poly_a || !poly_b) {
		return NULL;
	}

	struct term *next_a = poly_a;
	struct term *next_b = poly_b;
	struct term *next_target = make_term(0, 0);
	if (!next_target) {
		return NULL;
	}
	polynomial *target = next_target;

	while (next_a && next_b) {
		if (next_a->exp == next_b->exp) {
			next_target->next = make_term((next_a->coeff + next_b->coeff), next_a->exp);
			if (!next_target->next) {
				return NULL;
			}
			next_target = next_target->next;
			next_a = next_a->next;
			next_b = next_b->next;
		} else if (next_a->exp > next_b->exp) {
			next_target->next = make_term(next_a->coeff, next_a->exp);
			if (!next_target->next) {
				return NULL;
			}
			next_target = next_target->next;
			next_a = next_a->next;
		} else if (next_a->exp < next_b->exp) {
			next_target->next = make_term(next_b->coeff, next_b->exp);
			if (!next_target->next) {
				return NULL;
			}
			next_target = next_target->next;
			next_b = next_b->next;
		} else {
			next_a = next_a->next;
			next_b = next_b->next;
		}

	}
	/*Advance head node to remove the initially created (0,0) node*/
	next_target = target->next;
	free(target);
	return next_target;
}

/*Returns a newly-malloced polynomial that is the difference of the two arguments*/
polynomial *sub_poly(polynomial *poly_a, polynomial *poly_b)
{
	if (!poly_a || !poly_b) {
		return NULL;
	}

	struct term *next_a = poly_a;
	struct term *next_b = poly_b;
	struct term *next_target = make_term(0, 0);
	if (!next_target) {
		return NULL;
	}
	polynomial *target = next_target;

	while (next_a) {
		next_b = poly_b;
		while (next_b) {
			if (next_a->exp == next_b->exp) {
				next_target->next = make_term((next_a->coeff - next_b->coeff), next_a->exp);
				if (!next_target->next) {
					return NULL;
				}
				next_target = next_target->next;
				break;
			} else if (next_a->exp > next_b->exp) {
				next_target->next = make_term(next_a->coeff, next_a->exp);
				if (!next_target->next) {
					return NULL;
				}
				next_target = next_target->next;
				break;
			} else {
				next_b = next_b->next;
			}
		}
		next_a = next_a->next;
	}
	/*Advance head node to remove the initially created (0,0) node*/
	next_target = target->next;
	free(target);
	return next_target;
}

/*Multiplies two polynomials together*/
polynomial *multiply_poly(polynomial *poly_a, polynomial *poly_b)
{
	if (!poly_a || !poly_b) {
		return NULL;
	}

	struct term *next_a = poly_a;
	struct term *next_b = poly_b;
	struct term  *prev_node = NULL;
	struct term *next_target = make_term(0, 0);
	if (!next_target) {
		return NULL;
	}
	polynomial *target = next_target;

	int prod_coeff = 0, prod_exp = 0;

	while (next_a) {
		next_b = poly_b;
		while (next_b) {
			prod_coeff = next_a->coeff * next_b->coeff;
			prod_exp = next_a->exp + next_b->exp;

			next_target->next = make_term(prod_coeff, prod_exp);
			if (!next_target->next) {
				return NULL;
			}
			next_target = next_target->next;
		
			next_b = next_b->next;
		}
		next_a = next_a->next;
	}
	/*Advance head node to remove the initially created (0,0) node*/
	next_target = target->next;
	free(target);

	/*Add matching coefficients*/
	next_a = next_target;
	while (next_a) {
		next_b = next_a->next;
		prev_node = next_b;
		while (next_b) {
			if (next_a->exp == next_b->exp) {
				next_a->coeff += next_b->coeff;
				prev_node->next = next_b->next;
				free(next_b);
				prev_node = next_b;
				next_b = next_a->next;
			} else {
				prev_node = next_b;
				next_b = next_b->next;
			}
		}
		next_a = next_a->next;
	}

	/*Order the terms*/
	next_a = next_target;
	prev_node = NULL;
	while (next_a->next) {
		next_b = next_a->next;
		if (next_b->exp > next_a->exp) {
			//switch them
			next_a->next = next_b->next;
			next_b->next = next_a;
			if (prev_node) {
				prev_node->next = next_b;
			}
			next_a = next_target;
			prev_node = NULL;
		} else {
			prev_node = next_a;
			next_a = next_a->next;
		}
	}

	return next_target;
}

/*Returns true if the two arguments have the same terms; false otherwise*/
bool is_equal(polynomial *poly_a, polynomial *poly_b)
{
	if (!poly_a || !poly_b) {
		return NULL;
	}

	while(poly_a && poly_b) {
		if (poly_a->coeff != poly_b->coeff) {
			return false;
		} else if (poly_a->exp != poly_b->exp) {
			return false;
		}
		poly_a = poly_a->next;
		poly_b = poly_b->next;
	}

	return true;
}

/*Calls the function transform on each term of the polynomial*/
void apply_to_each_term(polynomial *poly, void (*transform)(struct term *))
{
	if (!poly) {
		return;
	}

	while (poly) {
		transform(poly);
		poly = poly->next;
	}
}

/*Evaluates the polynomial by substituting x in the variable of the polynomial*/
double eval_poly(polynomial *poly, double x)
{
	if (!poly) {
		return 0;
	}

	double ans = 0, term_value = 0;

	while (poly) {
		term_value = pow(x, poly->exp);
		term_value *= poly->coeff;

		ans += term_value;
		term_value = 0;		
		poly = poly->next;
	}

	return ans;
}

